<?php
/**
 * Plugin Name:     Biz Sites Etc. Events Calendar
 * Plugin URI:      https://business-websites-etc.com/event_post
 * Description:     Custom Event post with shortcode for a calendar
 * Author:          Phil Kane
 * Author URI:      https://business-websites-etc.com/phil-kane
 * Text Domain:     bwe-events-calendar
 * Domain Path:     /languages
 * Version:         0.1.0
 *
 * @package bwe_events_calendar
 */

namespace BWE\EventsCalendar;

use BWE\EventsCalendar\Post_Types\Custom_Event_Post;
use BWE\EventsCalendar\Calendar_Settings;
use BWE\EventsCalendar\Month;

if (!defined('ABSPATH')) {
	die('No direct access allowed');
}

if(!class_exists('Event_Post')) {
    class Event_Post
    {
        /**
         * Construct the plugin object
         */
        public function __construct()
        {
            // Initialize Settings

            // Register custom post types
            include_once sprintf("%s/post-types/custom_event_post.php", dirname(__FILE__));
            $Custom_Event_Post = new Custom_Event_Post();

            // Register custom taxonomies
            include_once sprintf("%s/taxonomy.php", dirname(__FILE__));

            // Add blocks
            include_once sprintf("%s/blocks/event_post_meta.php", dirname(__FILE__));

            // register new plant category
            add_action('block_categories_all', array(&$this, 'add_event_block_category'));

			include_once sprintf("%s/calendar_settings.php", dirname(__FILE__));
			$CalendarSettings = new Calendar_Settings();
			if ( is_admin() ) {
            	$plugin = plugin_basename(__FILE__);
            	add_filter("plugin_action_links_$plugin", array( $this, 'plugin_settings_link' ));
			}

			//add_filter( 'after_setup_theme', [ $this, 'add_it'], 10, 2 );
			//
			add_filter( 'pre_get_posts', [ $this, 'add_it'], 10, 2 );

        } // END public function __construct

		public function add_it($wp_query){

			if ( isset( $wp_query->query['post_type'] ) && ( 'event_post' === $wp_query->query['post_type'] ) )  {

				add_filter( 'wp_head', [ $this, 'add_structured_data'], 10, 2 );
			}

		}

 /**
         * Creats structured data for google search in the head of a page for job postings
         * <script type="application/ld+json">{"@context":"http:\/\/schema.org","@type":"JobPosting" ... </script>
         *
         * @return void
         */
        public function add_structured_data()
        {
			global $post;
			$event_title = $post->post_title;
			$event_description = $post->excerpt;

			$event_location = 'Third Place Books';
			$event_url = 'https://thirdplacebooks.com';
			$start_date = '2022-09-21T19:00';
			$end_date = '2022-09-21T20:00';

		//	prd($post,62);
            $content = [
                'name'             => $event_title,
                'description'      => $event_description,
				'location'         => $event_location,
				'location_url'     => $event_url,

                'start_date'        => $start_date,
				'end_date'          => $end_date,
                'street'           => '123 Main St',
                'city'             => 'Everett',
                'state'            => 'WA',
                'postal_code'      => '98201',
                'country'          => 'US',

           ];

            $title          = $content['name'];
            $description    = $content['description'];
			$event_location = $content['location'];
			$event_url      = $content['location_url'];

            $street         = $content['street'];
            $city           = $content['city'];
            $state          = $content['state'];
            $postal_code    = $content['postal_code'];
            $country        = $content['country'];

			$start_date     = $content['start_date'];
			$end_date       = $content['end_date'];

            $schema = [
                '@context'            => esc_url( 'http://schema.org' ),
                '@type'               => 'Event',
				'eventAttendanceMode' => 'https://schema.org/MixedEventAttendanceMode',
				'name'     => $event_title,
				'description' => $description,
				/*'performer' => $performer,
				'image' => $image,
				'eventStatus' => $event_status,
				'organizer'   => $organizer,
				'offers' => $offers,
				*/
                'location'    => [
					'@type'      =>  "VirtualLocation",
					'url'       => esc_url($event_url),

                    '@type'      =>  "Place",
					'name'       => esc_attr($event_location),
                    'address'    => [
                        '@type'           => 'PostalAddress',
                        'streetAddress'   => esc_attr( $street ),
                        'addressLocality'  => esc_attr( $city ),
                        'addressRegion'   =>  esc_attr( $state ),
                        'postalCode'      => esc_attr( $postal_code ),
                        'addressCountry'  => esc_attr( $country ),
                    ],

				],
				'startDate' => esc_attr($start_date),
				'endDate'   => esc_attr($end_date),
            ];
           ob_start();
           echo '<script type="application/ld+json">' . json_encode($schema) . '</script>';
           $output_string = ob_get_contents();
	       ob_end_clean();
           echo $output_string;
        }

        /**
         * Activate the plugin
         */
        public static function activate()
        {
            if (! current_user_can('activate_plugins') ) {
                return;
            }

            $plugin = isset($_REQUEST['plugin']) ? $_REQUEST['plugin'] : '';
            check_admin_referer("activate-plugin_{$plugin}");
            // Do nothing
        } // END public static function activate
        /**
         * Deactivate the plugin
         */
        public static function deactivate()
        {
            if (! current_user_can('activate_plugins') ) {
                return;
            }
			flush_rewrite_rules();
            $plugin = isset($_REQUEST['plugin']) ? $_REQUEST['plugin'] : '';
            check_admin_referer("deactivate-plugin_{$plugin}");

            /**
             *  Automatic flushing of the WordPress rewrite rules
             *  (usually needs to be done manually for new custom post types).
             *
             * @return void
             */
            function bizsitesetc_calendar_rewrite_flush()
            {
                my_custom_posttypes();
                flush_rewrite_rules();
            }
            register_activation_hook(__FILE__, 'bizsitesetc_calendar_rewrite_flush');
            // Do nothing
        } // END public static function deactivate

        // Add the settings link to the plugins page
        function plugin_settings_link($links)
        {
            $settings_link = '<a href="edit.php?post_type=event_post&page=bizsitesetc-calendar">Settings</a>';
            //$settings_link = '<a href="options-general.php?page=bizsitesetc-calendar">Settings</a>';
            array_unshift($links, $settings_link);
            return $links;
        }

        /**
         *  Adds the structured data blocks category to the Gutenberg categories.
         *
         * @param array $categories The current categories.
         *
         * @return array The updated categories.
         **/
        function add_event_block_category( $categories )
        {
            $categories[] = array(
            'slug'  => 'event-blocks',
            'title' => sprintf(
                /* translators: %1$s expands to Yoast. */
                __('%1$s Event Blocks', 'bizsitesetc_calendar'),
                'Biz Sites Etc'
            ),
            );
            return $categories;
        }

    } // END class Event_Post
} // END if(!class_exists('Event_Post'))
if(class_exists('BWE\EventsCalendar\Event_Post')) {
    // Installation and uninstallation hooks
	//if( is_user_logged_in() ){

    register_activation_hook(__FILE__, array('Event_Post', 'activate'));
    register_deactivation_hook(__FILE__, array('Event_Post', 'deactivate'));
    // instantiate the plugin class
    $Event_Post = new Event_Post();

	//register_uninstall_hook(__FILE__, 'pluginprefix_function_to_run');
}

