<?php
/**
 *
 *
 */

namespace BWE\EventsCalendar;

if (!defined('ABSPATH')) { die('No direct access allowed');
}

if(!class_exists('DaysOfWeek')) {
    class DaysOfWeek
    {

        function get_days()
        {
            $output = array('Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat');
            return $output;
        }
    }
}
if(!class_exists('CalendarDay')) {
    class CalendarDay
    {

        public function __construct($inmonth, $month, $day, $year)
        {
            $this->month = $month;
            $this->day = $day;
            $this->year = $year;
            $this->inmonth = $inmonth;
            //$this->{'number'} = $number;
            $this->text = '';
        }
        function get_month()
        {
            return $this->month;
        }
        function get_day()
        {
            return $this->day;
        }
        function get_year()
        {
            return $this->year;
        }
        function get_inmonth()
        {
            return $this->inmonth;
        }
        function get_text()
        {
            return $this->text;
        }
        function set_text($text)
        {

            $eventsday  = $text['day'];
            $eventlink  = $text['link'];
            $eventtitle = $text['title'];
            $eventcopy  = $text['copy'];
            $eventtime  = $text['time'];
            $eventIcon  = $text['icon'];

            if(wp_http_validate_url($eventlink)) {
                $calendar_post = ' ' . $eventIcon . '<a href="' . $eventlink . '">  ' . $eventtitle . ', </a> ';
            } else {
                $calendar_post =  ' ' . $eventtitle . ', ' ;
            }
            $calendar_post .= __($eventcopy, 'bizsitesetc_calendar') . ', ';
            $calendar_post .= $eventtime;

            return $this->text = $calendar_post;
        }
    }
}
