<?php
/**
 *
 *
 */

namespace BWE\EventsCalendar;

if (!defined('ABSPATH')) die('No direct access allowed');

if (!class_exists('Month')){
	class Month
	{
		function get_months() {
			$output = array(1 => 'January', 2 =>'February', 3 =>'March', 4 =>'April', 5 =>'May',
			6 =>'June', 7 =>'July', 8 =>'August', 9 =>'September', 10 =>'October', 11 =>'November', 12 =>'December');
			return $output;
		}

		function get_last_month( $month, $year ){

			$lastmonth = $month - 1;
			//prd($lastmonth);
			$lastyear = $year;
			if ( $lastmonth == 0 ) {
				$lastmonth = 12;
				$lastyear = $year - 1;
			}
			return array( $lastmonth, $lastyear );
		}

		function get_next_month( $month, $year ){

			$nextmonth = $month + 1;
			$nextyear = $year;
			if ( $nextmonth > 12 ) {
				$nextmonth = 1;
				$nextyear += 1;
			}
			return array( $nextmonth, $nextyear );
		}
		/*
		function get_last_month( $month, $year ){

			$lastmonth = $month - 1;
			//prd($lastmonth);
			$lastyear = $year;
			if ( $lastmonth < 0 ) {
				$lastmonth = 11;
				$lastyear = $year - 1;
			}
			return array( $lastmonth, $lastyear );
		}

		function get_next_month( $month, $year ){

			$nextmonth = $month + 1;
			$nextyear = $year;
			if ( $nextmonth > 11 ) {
				$nextmonth = 0;
				$nextyear = $year + 1;
			}
			return array( $nextmonth, $nextyear );
		}
		*/
	}
}
