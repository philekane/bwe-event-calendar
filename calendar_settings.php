<?php
/**
 * Calendar settings for bwetc event posts
 */

namespace BWE\EventsCalendar;

use BWE\EventsCalendar\CalendarDay;

if (!defined('ABSPATH')) {
	die('No direct access allowed');
}

if (!class_exists('Calendar_Settings')) {

    class Calendar_Settings
    {
        /**
         * Construct the plugin object
         */
        public function __construct()
        {
            $this->daysOfWeek = array('Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat');

            // allow editor to edit settings
           // $role = get_role('editor');
           //$role->add_cap('manage_options');

            // register actions

			if ( is_admin() ) {
				add_action('admin_init', array(&$this, 'adminInit'));
                add_action('admin_menu', array(&$this, 'addMenu'));
            }
			add_action('init', array(&$this,'bizsitesetcCalendarSetup'));
			add_action('init', array(&$this,'bwetc_register_param'));


			//    add_action('admin_menu', array(&$this, 'add_menu'));
        } // END public function __construct

        function bwetc_register_param()
        {
            global $wp;
            $wp->add_query_var('event');
            $wp->add_query_var('action');
        }

        /**
         * Registers shortcode for calendar
         *
         * @return html form of taxonomies and ters to search custom post
         */
        function bizsitesetcCalendarSetup()
        {
            add_shortcode('bizSitesEtcEventsCalendar', array( &$this , 'bizsitesetcCreateCalendar'));
            add_action('wp_enqueue_scripts', array( &$this, 'add_calendar_styles' ) );
            add_action('wp_enqueue_scripts', array( &$this, 'add_calendar_js' ) );

			//add_filter( 'is_post_type_viewable', [ $this, 'wporg_books_is_not_a_viewable_post_type'], 10, 2 );
			//add_action( 'pre_get_posts', [ $this, 'add_it'] );
        }

		function wporg_books_is_not_a_viewable_post_type( $is_viewable, $post_type ) {
			global $post;
			if ( is_singular( 'events' ) ) {
				prd('conditional content/code');
			}
			if ( __( 'Events', 'my-plugin' ) === $post_type->label ) {
				prd('false');
			}
			prd(get_post_type(),71);
		}

		public function add_it($post){
			//global $post;
			//prd($post,66);
			global $wp_query;
			if ( isset( $wp_query->query['post_type'] ) && ( 'Event' === $wp_query->query['post_type'] ) )  {
				prd('global styles');
			}
			if ( is_front_page() && is_home() ) {
				prd('Default homepage');
			  }elseif (is_singular('wp_global_styles')){
				prd('custom post');
			  } elseif ( is_front_page() ) {
				prd('static homepage');
			  } elseif ( is_home() ) {
				prd('blog page');
			  } else {
				prd('everything else');
			  }


			if(isset($wp_query->query['post_type']) && 'events' === $wp_query->query['post_type']){
				prd('yep');
				add_filter( 'wp_head', [ $this, 'add_structured_data'], 10, 2 );
			}
		}
		 /**
             * Add css styles for calendar
             *
             * @return void
             */
            function add_calendar_styles()
            {
                wp_enqueue_style('bwetc-calendar-style.min', plugin_dir_url(__FILE__) . 'css/style.min.css', array(), '20180915', 'all');
            }
            /**
             * Add js for calendar
             *
             * @return void
             */
            function add_calendar_js()
            {
                wp_enqueue_script('calendar', plugin_dir_url(__FILE__) . 'js/calendar.js');
            }

        /**
         * Get taxonomy for custom post
         *
         * @param [event] $custom_post
         *
         * @return taxonomy
         */
        function getTaxonomyObjects($custom_post)
        {
            $taxonomy_objects = get_object_taxonomies($custom_post, 'objects');
            foreach ($taxonomy_objects as $key => $tax) {

                if ($tax->object_type[0] == $custom_post) {
                       return $tax->name;
                }
            }
        }

        /**
         * Get the terms for the taxonomy
         *
         * @param [type] $taxonomy
         *
         * @return The terms of the taxonomy
         */
        function getTaxonomyTermsMenu($taxonomy)
        {
            $terms = get_terms(
                array( 'taxonomy' => $taxonomy,
                'hide_empty' => false,
                'orderby' => 'name',
                'order' => 'ASC',
                'number' => false,
                'parent' => 0,
                )
            );
            return $terms;
        }

        /**
         * Get the icon for the event-type
         *
         * @param [type] $event_type
         *
         * @return The hex icon saved in the database for the term
         */
        function getEventsSelection()
        {
            $showSelection = get_option('bwetc_calendar_show_selections');

            if ($showSelection) {
                //get event types for drop down selection
                $eventTypes = array();
                $event_types =  $this->getTaxonomyTermsMenu('event-types');

				//$event_locations = array();
                //$event_locations =  $this->getTaxonomyTermsMenu('event-locations');
				//pr('here',136);
				//prd($event_types, 137);

                foreach ( $event_types as $event_type ) {
                    $eventTypes[] = $event_type->slug;
                }
                if (count($eventTypes) < 2) {
                    return;
                }
                $output = '<div class="sortForm">
				<form id="eventform" method="post">
					<fieldset>
						<legend></legend>';
                $output .= '<label for="eventtype_id">' . __('Event Calendars', 'bizsitesetc_calendar') . '</label>';
                $output .= '<select id="eventtype_id" name="eventtype" onchange="eventchange()">';
                $output .= '<option value="none" >' . __('Select Event', 'bizsitesetc_calendar') . '</option>';
                $output .= '<option value="all" >' . __('All Events', 'bizsitesetc_calendar') . '</option>';
                foreach ($eventTypes as $eventType) {
                    $output .= '<option value="' . $eventType . '">' . __(ucfirst($eventType), 'bizsitesetc_calendar')  . __(' Events', 'bizsitesetc_calendar') . '</option>';
                }
                $output .= '</select>';
                $output .= '</fieldset></form></div>';

                return $output;
            }
        }

        /**
         * Get the icon for the event-type
         *
         * @param [type] $event_type
         *
         * @return The hex icon saved in the database for the term
         */
        function getEventIcon($event_type)
        {
            $icon = get_option('bwetc_calendar_' . $event_type . '_icon');
            if ($icon) {
                return $icon;
            } else {
                return '&#x1f4c5';
            }
        }

        /**
         * Create css style for calendar content font-size,
         * color depending on event-type
         *
         * @param [css] $style
         *
         * @return calendar css style
         */
        function getStyle($style)
        {
            if(empty($style)) {
                return;
            }
            $calendar_width = get_option('bwetc_calendar_style' . $style . '-width');
            $font_color = get_option('bwetc_calendar_style' . $style . '-content_color');
            $font_size = get_option('bwetc_calendar_style' . $style . '-font_size');

            $output ='<style scoped>
			.calendar-container{width:' . $calendar_width . '%;}
			.calendar-content{color:' . $font_color .';font-size:' . $font_size .';}
			.calendar-content a{color:' . $font_color .';font-size:' . $font_size .';}
			</style>';

            return $output;
        }

         /**
         * Creates the calendar
         *
         * @param [array with event and style ] $atts
         *
         * @return the calendar
         */
        function bizsitesetcCreateCalendar($atts)
        {
		//	prd('here',215);
            include_once sprintf("%s/days.php", dirname(__FILE__));

            if (isset($_GET['calendar-year'])) {
                $thisyear = $_GET['calendar-year'];
            }
            if (isset($_GET['month'])) {
                $thismonth = $_GET['month'];
            }

            $today = getdate();
            $thiscalendaryear = $today['year'];
            $thiscalendarmonth = $today['mon'] ;

            if (isset($_GET['calendar-year'])) {
                $calendarmonth = $thismonth;
                $calendaryear = $thisyear;

            } else {

                $today = getdate();
                $calendaryear = $today['year'];
                $calendarmonth = $today['mon'] ;
            }
            include_once sprintf("%s/months.php", dirname(__FILE__));
            $month = new Month();
            $months = $month->get_months();

            $day_names = $this->daysOfWeek;

            $days = $this->makeCalendarDays($calendarmonth, $calendaryear);

            $style = ''; //initialize css for calendar
            $calendarType = ''; //initialize calendarType
            // If user clicked on a term/event-type use that for the
            //$caldendarType else all
            if ($atts) {
                if (isset($atts['event'])) {
                    $calendarType =    $atts['event'];
                } else {
                    $calendarType = '';
                }
                if (isset($atts['style'])) {
                    $style = $atts['style'];
                }
            }

            if (isset($_POST['eventtype']) ) {
                    $calendarType = $_POST['eventtype'];
            }
            if (isset($_GET['event']) && ($_GET['action'] == 'edit')) {
                $calendarType = $_GET['event'];
            }

            if ($calendarType == 'all' || $calendarType == '' ) {
                    //get all taxonomies and terms for event_post taxonomy event-types
                $terms = $this->getTaxonomyTermsMenu('event-types');
            } else {

                    $terms[0]['slug'] = $calendarType;
                    $slug = $calendarType;
            }

            $table_caption = ($calendarType)? ucfirst($calendarType) . ' Events' : 'All Events' ;

            $events_array = array();
            foreach ($terms as $term) {
                if (is_array($term)) {
                    $slug = $term['slug'];
                } else {
                    $slug = $term->slug;
                }
                $args = array(
					'post_type'      => 'event_post',
					'meta_key'       => 'event_post_date',
					'orderby'        => 'meta_value', // we will sort posts by name
					'order'          =>'ASC', // ASC or DESC
					'tax_query'      => array(
						array(
						'taxonomy' => 'event-types',
						'field'    => 'slug',
						'terms'    => $slug,
						),
                	),
					'meta_query'  => array(         // restrict posts based on meta values
					'key'     => 'event_post_date',  // which meta to query
					'value'   => date('Y') . '-01-01',  // value for comparison
					'compare' => '>=',          // method of comparison
					'type'    => 'DATE'         // datatype, we don't want to compare the string values
					), // end meta_query array
                );
                 $query = new \WP_Query( $args );

                if ($query->have_posts() ) {

                    while ( $query->have_posts() ) {
                        $query->the_post();

                        $event_date =   get_post_meta(get_the_ID(), 'event_post_date', true);
                        $id         = get_the_ID();

                        $calendarcopy    =   get_post_meta(get_the_ID(), 'event_post_calendarcopy', true);
                        $event_post_time =   get_post_meta( get_the_ID(), 'event_post_time', true);
                        $event_time      = date( 'g:i a', strtotime( $event_post_time ) );
                        $event_day       = date( 'd', strtotime( $event_date ) );

                        $event_month = date('m', strtotime($event_date));
                        if ($calendarmonth == $event_month) {

                            $events_array[$id]['day'] = $event_day;
                            $events_array[$id]['link'] = esc_url(get_permalink());
                            $events_array[$id]['title'] = get_the_title();
                            $events_array[$id]['copy'] = $calendarcopy;
                            $events_array[$id]['time'] = $event_time;

                            $show_icon = get_option('bwetc_calendar_' . $slug . '_show_icon');
                            if (1 == $show_icon) {
                                $events_array[$id]['icon'] = $this->getEventIcon($slug);
                            } else {
                                $events_array[$id]['icon'] = '';
                            }
                        }
                    }
                         wp_reset_postdata();
                }
            }
            if ( isset( $events_array ) ) {
                usort($events_array, array( &$this, 'sort_time' ) );
            }

            $output = $this->getStyle($style);
            $output .= '<div class="calendar-container" tabindex="0" role="group" aria-labelledby="caption">';
            $output .= '<table class="calendar">';

            if ($calendarmonth != $thiscalendarmonth && $calendaryear != $thiscalendaryear || $calendarmonth != $thiscalendarmonth && $calendaryear == $thiscalendaryear ) {
                 $todaysCalendar = '<a href="?calendar-year=' . $thiscalendaryear . '&month=' .  $thiscalendarmonth  . '">' . __('Today\'s Calendar', 'bizsitesetc_calendar') . '</a>';
            } else {
                $todaysCalendar = '';
            }

            $output .= '<caption id="caption">' . __( $table_caption, 'bizsitesetc_calendar' ) . ' ' . $todaysCalendar . '</caption>';

            if (!isset($atts['event'])) {
                $output .= $this->getEventsSelection();
            }

            $output .='<tr><th colspan="7" scope="colgroup" class="calendar-title" >';
            $nextmonthArray = $month->get_next_month($calendarmonth, $calendaryear);
            $lastmonthArray = $month->get_last_month($calendarmonth, $calendaryear);

            $nextmonth = $nextmonthArray[0];
            $nextyear = $nextmonthArray[1];
            $lastmonth = $lastmonthArray[0];
            $lastyear = $lastmonthArray[1];
            $output .= '<a href="?calendar-year=' . $lastyear . '&month=' . $lastmonth  . '"> &lt;&lt; </a>';
            $output .= __($months[$calendarmonth], 'bizsitesetc_calendar') . ' ' . $calendaryear;
            $output .= '<a href="?calendar-year=' . $nextyear . '&month=' . $nextmonth  . '"> &gt;&gt; </a>';
            $output .= '</th></tr>';

            $output .= '<tr>';
            foreach ( $day_names as $day) {
                $output .= '<th scope="col" class="calendar-day-title">' . __($day, 'bizsitesetc_calendar')  . '</th>';
            }
            $output .= '</tr>';
            $p = 0;
            $adCount = 0;
            foreach ( $days as $d ) {
                if ($p  == 0) {
                    $output .= '<tr>';
                }

                $day_style = $d->get_inmonth() ? "calendar-day" : "calendar-outmonth-day";
                $output .= '<td class="' . $day_style . '" >';
                $output .= '<div class="calendar-day-number">';
                $output .= $d->get_day();
                $output .= '</div>';
                $output .= '<div class="calendar-content">';
                if (isset($events_array)) {
					foreach ( $events_array as $key => $event ) {
								$eventday = $event['day'];
						if ($d->get_day() == $eventday ) {
							$this->setCalendarText($days, $calendarmonth, $eventday, $calendaryear, $event);
							$output .= $d->get_text();
						}
					}
                }

                // $output .= $d->get_text();
                $output .= '</div>';
                $output .= '</td>';
                $p += 1;
                if ($p == 7) {
                    $p = 0;
                }
            }
            $output .= '</tr>';
            $output .= '</table></div>';



            return  $output;
        }


		 /**
         * Creats structured data for google search in the head of a page for job postings
         * <script type="application/ld+json">{"@context":"http:\/\/schema.org","@type":"JobPosting" ... </script>
         *
         * @return void
         */
        public function job_posting_structured_data()
        {

			$content = [
                'title'                => 'WordPress Developer',
                'description'          => 'Seeking a WordPress Developer to join our The selected candidate will engage with the Digital Marketing team and various profit centers to build and maintain our internet presence. They will participate in the design, coding, testing, debugging, and deployment of new and existing websites. They will resolve customer complaints and respond to suggestions for improvements and enhancements. Candidate should be able to work well with co-located and distributed team member',
                'employment_type'      => 'Full_Time',
                'organization'         => ['name' => 'BlueStone Staffing', 'url' => 'https://bluestonestaffing.com'],
                'posted_date'          => '2022-07-01 00-00-0000',
                'valid_through'        => '2022-08-01 00-00-0000',
                'street'               => '123 Main St',
                'city'                 => 'Everett',
                'state'                => 'WA',
                'postal_code'          => '98201',
                'country'              => 'US',
                'job_location_type'    => 'TELECOMMUTE',
                'job_location_country' => 'USA',
                'unit_text'            => 'HOUR',
                'value'                => 40.00,
                //'min_value'          => 40.00,
                //'max_value'          => 50.00,

           ];

            $title             = $content['title'];
            $description       = $content['description'];
            $employment_type   = $content['employment_type'];
            $organization_name = $content['organization']['name'];
            $organization_url  = $content['organization']['url'];
            $posted_date       = $content['posted_date'];
            $valid_through     = $content['valid_through'];

            $job_location_type    = $content['job_location_type'];
            $job_location_country = $content['job_location_country'];
            $street               = $content['street'];
            $city                 = $content['city'];
            $state                = $content['state'];
            $postal_code          = $content['postal_code'];
            $country              = $content['country'];

            $unit_text            = $content['unit_text'];
            $value                = $content['value'];
            //$min_value          = $content['min_value'];
            //$max_value          = $content['max_value'];

            $schema = [
                '@context'           => esc_url( 'http://schema.org' ),
                '@type'              => 'JobPosting',
                'title'              => esc_attr( $title ),
                'description'        => esc_attr( $description ),
                'hiringOrganization' => [
                    '@type'          => 'Organization',
                    'name'           => esc_attr( $organization_name ),
                    'sameAs'         => esc_url( $organization_url ),
                    'logo'           => '/wp/wp-content/uploads/2016/11/cropped-logo.png'

                ],
                'employmentType' => esc_attr( $employment_type ),
                'datePosted'     => esc_attr( $posted_date ),
                'validThrough'   => esc_attr( $valid_through ),
                'baseSalary'     => [ //optional
                    '@type'      => 'MonetaryAmount',
                    'currency'   => 'USD',
                    'value'      => [
                      '@type'    => 'QuantitativeValue',
                      'value'    => esc_attr( $value ),
                      //'minValue' => 40.00,
                      //'maxValue' => 50.00,
                      'unitText' => esc_attr( $unit_text ),
                    ]
                ],
                'jobLocation'    => [
                    '@type'      =>  "Place",
                    'address'    => [
                        '@type'           => 'PostalAddress',
                        'streetAddress'   => esc_attr( $street ),
                        'addressLocality'  => esc_attr( $city ),
                        'addressRegion'   =>  esc_attr( $state ),
                        'postalCode'      => esc_attr( $postal_code ),
                        'addressCountry'  => esc_attr( $country )
                    ],
                ],
                'applicantLocationRequirements' => [
                    '@type' => 'Country',
                    'name'  => esc_attr( $job_location_country ),
                ],
                  'jobLocationType' => esc_attr( $job_location_type )
            ];
           ob_start();
           echo '<script type="application/ld+json">' . json_encode($schema) . '</script>';
           $output_string = ob_get_contents();
	       ob_end_clean();
           echo $output_string;
        }

		/**
		 * Sort the times of the events for the day
		 *
		 * @param [time] $a
		 * @param [time] $b
		 *
		 * @return array that has been sorted by time
		 */
		function sort_time( $a, $b )
		{
			return strcmp( $a["time"], $b["time"] );
		}
        /**
         * Sets the content for the event on the calendar
         *
         * @param [days]          $days
         * @param [month]         $month
         * @param [d]             $d
         * @param [year]          $year
         * @param [event content] $text
         *
         * @return the content for the calendar event
         */
        public function setCalendarText($days, $month, $d, $year, $text)
        {
            foreach ($days as $calendarDay) {

                if ($calendarDay->get_day() == $d
                    && $calendarDay->get_month() == $month
                    && $calendarDay->get_year() == $year
                ) {
                    $calendarDay->set_text($text);
                }
            }
        }
        /**
         * Create the days for the month and year using the julian day
         * and the cal_gregorian
         */
        public function makeCalendarDays($thismonth, $thisyear)
        {
            $month = new Month();
            $nextmonthArray = $month->get_next_month($thismonth, $thisyear);
            $lastmonthArray = $month->get_last_month($thismonth, $thisyear);

            $nextmonth = $nextmonthArray[0];
            $nextyear = $nextmonthArray[1];
            $lastmonth = $lastmonthArray[0];
            $lastyear = $lastmonthArray[1];

            $days_in_last_month = cal_days_in_month(CAL_GREGORIAN, $lastmonth, $lastyear);
			$julianday = cal_to_jd(CAL_GREGORIAN, $thismonth, 1, $thisyear);
            $day = jddayofweek($julianday);
            $days_in_month = cal_days_in_month(CAL_GREGORIAN, $thismonth, $thisyear);
            $days = array();

            for ( $d = 0; $d < $day; $d++ ) {
                $days[] =  new CalendarDay(0, $lastmonth, $days_in_last_month - ($day - $d) +1, $lastyear);
            }

            for ( $d = 1; $d <= $days_in_month; $d++ ) {
                $days[] = new CalendarDay(1, $thismonth, $d, $thisyear);
            }

            $left = ( ( floor(( $day + $days_in_month ) / 7) + 1 ) * 7 ) - ( $day + $days_in_month );
            for ($d = 0; $d < $left; $d++) {
                $days[] = new CalendarDay(0, $nextmonth + 1, $d+1, $nextyear);
            }

            return $days;
        }


        /**
         * Register the fields for the form to set up the calendar settings
         * and event icons
         *
         * @param [event type] $eventType
         *
         * @return fields for calendar setting form
         */
        function getCalendarFields($eventType)
        {
            add_settings_section(
                'bizsitesetc_event-section',  // ID
                __('Edit ' . ucfirst($eventType) . ' Calendar', 'bizsitesetc_calendar'),  // Title
                array(&$this, 'bwetcCalendarEventSection'), // Callback
                'bizsitesetc_event' // Page
            );

            register_setting(
                'bizsitesetc_event-group', // Option group
                'bwetc_calendar_' . $eventType . '_show_icon', // Option name
                '' // Sanitize
            );

            add_settings_field(
                'bwetc_calendar_' . $eventType . '_show_icon', // ID
                __('Show icon on calendar', 'bizsitesetc_calendar'), // Title
                array(&$this, 'settingsFieldInputCheckbox'), // Callback
                'bizsitesetc_event', // Page
                'bizsitesetc_event-section', // Section
                array(
                'field' => 'bwetc_calendar_' . $eventType . '_show_icon',
                'default' => '',
                'type' => 'checkbox',
                )
            );
            // Hidden field not registered
            add_settings_field(
                'bwetc_calendar_event',
                '',
                array(&$this, 'settingsFieldHiddenEvent'),
                'bizsitesetc_event',
                'bizsitesetc_event-section',
                array(
                'field' => 'bwetc_calendar_event',
                'default' => $eventType,
                'type' => 'hidden',
                )
            );

            register_setting('bizsitesetc_event-group', 'bwetc_calendar_' . $eventType . '_icon');
            add_settings_field(
                'bwetc_calendar_' . $eventType . '_icon',
                '',
                array(&$this, 'settingsFieldInputTermTable'),
                'bizsitesetc_event',
                'bizsitesetc_event-section',
                array(
                'field' => 'bwetc_calendar_' . $eventType . '_icon',
                'default' => $eventType,
                'type' => 'text',
                )
            );
        }
        /**
         * Register the fields for the form to set up the css style settings
         *
         * @param [css style] $style
         *
         * @return fields for css style for calendar form
         */
        function getStyleFields($style)
        {
            if (empty($style)) {
                $option = get_option('bwetc_calendar_style_count');
                if (empty($option)) {
                    $styleCount = $option +1;
                } else {
                    $styleCount = $option +1;
                }
            } else {
                $styleCount = $style;
            }

            add_settings_section(
                'bizsitesetc_style-section',
                '	',
                array(&$this, 'bwetcCalendarStyleSection'),
                'bizsitesetc_style'
            );

            register_setting('bizsitesetc_style-group', 'bwetc_calendar_style_count');
            add_settings_field(
                'bwetc_calendar_style_count',
                '',
                array(&$this, 'settingsFieldHiddenEvent'),
                'bizsitesetc_style',
                'bizsitesetc_style-section',
                array(
                'field' => 'bwetc_calendar_style_count',
                'default' => $styleCount,
                'type' => 'hidden',
                )
            );

            register_setting('bizsitesetc_style-group', 'bwetc_calendar_style' . $styleCount . '-width');
            add_settings_field(
                'bwetc_calendar_style' . $styleCount . '-width',
                __('Calendar Width for Style ' . $styleCount . ' (50%-100%)', 'bizsitesetc_calendar'),
                array(&$this, 'settingsFieldInputRange'),
                'bizsitesetc_style',
                'bizsitesetc_style-section',
                array(
                'field' => 'bwetc_calendar_style' . $styleCount . '-width',
                'default' => '',
                'type' => 'range',
                )
            );

            register_setting('bizsitesetc_style-group', 'bwetc_calendar_style' . $styleCount . '-content_color');
            add_settings_field(
                'bwetc_calendar_style' . $styleCount . '-content_color',
                __('Color for the calendar content for ' .  'Style ' . $styleCount . ' (be mindful of a contrasting color.)', 'bizsitesetc_calendar'),
                array(&$this, 'settingsFieldInputColor'),
                'bizsitesetc_style',
                'bizsitesetc_style-section',
                array(
                'field'   => 'bwetc_calendar_style' . $styleCount . '-content_color',
                'default' => '',
                'type'    => 'text',
                'input_class'   => 'my-color-field'
                )
            );

            register_setting('bizsitesetc_style-group', 'bwetc_calendar_style' . $styleCount . '-font_size');
            add_settings_field(
                'bwetc_calendar_style' . $styleCount . '-font_size',
                __('Font size for Calendar content for style ' . $styleCount, 'bizsitesetc_calendar'),
                array(&$this, 'settingsFieldInputRadio'),
                'bizsitesetc_style',
                'bizsitesetc_style-section',
                array(
                'field' => 'bwetc_calendar_style' . $styleCount . '-font_size',
                'default' => '',
                'type' => 'radio',
                )
            );

            add_settings_field(
                'bwetc_calendar_style',
                __('Font size for Calendar content for style ' . $styleCount, 'bizsitesetc_calendar'),
                array(&$this, 'settingsFieldHiddenEvent'),
                'bizsitesetc_style',
                'bizsitesetc_style-section',
                array(
                'field' => 'bwetc_calendar_style',
                'default' => '',
                'type' => 'hidden',
                )
            );
        }
        /**
         * Calendar settings for the table
         *
         * @return setting
         */
        function getCalendarSettingsTable()
        {
            add_settings_section(
                'bizsitesetc_table-section',
                '	',
                array(&$this, 'bwetcCalendarTablesSection'),
                'bizsitesetc_table'
            );

            register_setting('bizsitesetc_table-group', 'bwetc_calendar_show_selections');
            add_settings_field(
                'bwetc_calendar_show_selections',
                __('Show events selection on calendar', 'bizsitesetc_calendar'),
                array(&$this, 'settingsFieldInputCheckbox'),
                'bizsitesetc_table',
                'bizsitesetc_table-section',
                array(
                'field' => 'bwetc_calendar_show_selections',
                'default' => '',
                'type' => 'checkbox',
                )
            );
            //    $this->getStyleFields('');
        }
		/**
             * Create color picker for the form
             *
             * @param [hook] $hook_suffix
             *
             * @return color picker
             */
            function bwe_enqueue_color_picker( $hook_suffix )
            {
                // first check that $hook_suffix is appropriate for your admin page
                wp_enqueue_style('wp-color-picker');
                wp_enqueue_script('my-script-handle', plugins_url('js/admin.js', __FILE__), array( 'wp-color-picker' ), false, true);
            }
			 /**
             * Add the calendar styles to the page
             *
             * @return void
             */
            function add_admin_calendar_styles()
            {
                wp_enqueue_style('admin-styles', plugin_dir_url(__FILE__) . 'css/admin.style.css', array(), '20180915', 'all');
            }
        /**
         * Hook into WP's adminInit action hook
         * Fields - calendar name, width, font, colors?
         */
        public function adminInit()
        {
            //$options = wp_load_alloptions();
            //prd($options);
            /*
            foreach ( wp_load_alloptions() as $option => $value ) {
            if ( strpos( $option, 'bwetc_calendar_style' ) === 0 ) {
            //prd($option);
            //delete_option( $option );
            }
        		}
            */
            add_action('admin_enqueue_scripts', array( &$this, 'bwe_enqueue_color_picker' ) );
            add_action('admin_enqueue_scripts', array( &$this, 'add_admin_calendar_styles' ) );

            /**
             *  If editing individual event calendar
             */

            if (isset($_POST['bwetc_calendar_event'])) {
                $eventType = $_POST['bwetc_calendar_event'];
            } else {
                if (isset($_GET['event']) ) {
                    $eventType = $_GET['event'];
                    $this->getCalendarFields($eventType);
                } else {
                    $eventType = '';
                }
            }

            $this->getCalendarFields($eventType);

            if (isset($_POST['bwetc_calendar_style_count'])) {
                $style = $_POST['bwetc_calendar_style_count'];
                $this->getStyleFields($style);
            } else {
                if (isset($_GET['style']) ) {
                    $style = $_GET['style'];
                    $this->getStyleFields($style);
                } else {
                    $this->getStyleFields('');
                }
            }

            $this->getCalendarSettingsTable();

            // Possibly do additional adminInit tasks
        } // END public static function activate
        /**
         * Adds style section
         *
         * @return form
         */
        public function bwetcCalendarStyleSection()
        {
            //echo __('Edit Calendar', 'bizsitesetc_calendar');
            // Think of this as help text for the section.
        }
        /**
         * Adds calendar events section
         *
         * @return form
         */
        public function bwetcCalendarEventSection()
        {
            //echo __('Edit Calendar', 'bizsitesetc_calendar');
            // Think of this as help text for the section.
        }
        /**
         * Adds calendar tables section
         *
         * @return form
         */
        public function bwetcCalendarTablesSection()
        {
            //echo __('Edit calendars', 'bizsitesetc_calendar');
            $terms = $this->getTaxonomyTermsMenu('event-types');

            $output = '<div id="calendars_table" >';
            $output .= '<table class="calendars_table" border="1">';
            $output .= '<caption>' . __('Calendars', 'bizsitesetc_calendar') . '</caption>';
            $output .= '<tbody>';
            $output .= '<tr><th scope="col"  class="admin_calendar_row">' . __('Calendar', 'bizsitesetc_calendar') .
            '</th><th scope="col"  class="calendar-day-title">' . __('shortcode', 'bizsitesetc_calendar') . '</th>
					   <th scope="col"  class="calendar-day-title">' . __('Author', 'bizsitesetc_calendar') . '</th>
					   <th scope="col"  class="calendar-day-title">' . __('Date', 'bizsitesetc_calendar') . '</th>
					   <th scope="col"  class="calendar-day-title">' . __('Date Modified', 'bizsitesetc_calendar') . '</th>
					   <th scope="col"  class="calendar-day-title">' . __('Edit', 'bizsitesetc_calendar') . '</th></tr>';
            $output .= '<tr>';
            $output .= '<th scope="row" class="admin_calendar_row">' . __('Admin', 'bizsitesetc_calendar') . '</th>';
            $output .= '<td></td><td></td><td></td><td></td><td>' . __('Edit Calendar', 'bizsitesetc_calendar') . '</td>';
            $output .= '</tr>';
            $output .= '<tr>';
            $output .= '<th scope="row" class="admin_calendar_row">' . __('All Events', 'bizsitesetc_calendar') . '</th>';
            $output .= '<td>[bizSitesEtcEventsCalendar style=0]</td><td></td><td></td><td></td><td>' . '<a href="/wp-admin/options.php?page=edit-calendar&action=edit&event=all">' . __('Edit Calendar', 'bizsitesetc_calendar') . '</a></td>';
            $output .= '</tr>';

            foreach ($terms as $term) {

                $output .= '<tr>';
                $output .= '<th scope="row" class="admin_calendar_row">' . $term->name . '</th>';
                $output .= '<td>[bizSitesEtcEventsCalendar event=' . $term->slug . ' style=0]</td><td></td><td></td><td></td><td><a href="/wp-admin/options.php?page=edit-calendar&action=edit&event=' . $term->slug . '">' . __('Edit Calendar', 'bizsitesetc_calendar') . '</a></td>';
                $output .= '</tr>';
            }
            $output .= '</tbody></table>';
            echo $output;

            $styleCount = get_option('bwetc_calendar_style_count');
            echo '<div><button type="button" aria-pressed="true" ><a href="/wp-admin/options.php?page=edit-style&action=edit&style=' . ($styleCount + 1) . '">Add New Style</a></button></div>';

            if ($styleCount > 0 ) {
                $styleOutput = '<div id="styles_table" >';
                $styleOutput .= '<table class="styles_table" border="1">';
                $styleOutput .= '<caption>' . __('Styles', 'bizsitesetc_calendar') . '</caption>';
                $styleOutput .= '<tbody>';
                $styleOutput .= '<tr><th scope="col"  class="admin_calendar_row">' . __('Style', 'bizsitesetc_calendar') .
                '</th><th scope="col"  class="calendar-day-title">' . __('Width', 'bizsitesetc_calendar') . '</th>
							<th scope="col"  class="calendar-day-title">' . __('Color', 'bizsitesetc_calendar') . '</th>
							<th scope="col"  class="calendar-day-title">' . __('Font Size', 'bizsitesetc_calendar') . '</th>
							<th scope="col"  class="calendar-day-title">' . __('Edit Style', 'bizsitesetc_calendar') . '</th>';

                //    $styleOutput .= '<tr>';
                for ($style = 1; $style <= $styleCount; $style++) {
                    $styleWidth = get_option('bwetc_calendar_style' . $style . '-width');
                    $styleColor = get_option('bwetc_calendar_style' . $style . '-content_color');
                    $styleFontSize = get_option('bwetc_calendar_style' . $style . '-font_size');

                    $styleOutput .= '<tr><td>Style=' . $style     . '</td><td>' . $styleWidth . '%</td>';
                    $styleOutput .= '<td style="color:' . $styleColor . ';">' . $styleColor . '</td><td>' . $styleFontSize . '</td>';
                    $styleOutput .= '<td><a href="/wp-admin/options.php?page=edit-style&action=edit&style=' . $style . '">Edit </td></tr>';
                }

                $styleOutput .= '</tbody></table>';
                echo $styleOutput;
            }
        }
        /**
         * This function provides hidden inputs for settings fields for styles
         *
         * @param [array for field name, default value, input type] $args
         *
         * @return input element for form
         */
        public function settingsFieldHiddenEvent($args)
        {
            // Get the field name from the $args array
            $field = $args['field'];
            $default = $args['default'];
            $type = $args['type'];
            // Get the value of this setting
            $value = get_option($field);
            //add 1 to value which is the number of styles saved (style_count)
            $value = $default;

            echo sprintf('<input type="%s" name="%s" id="%s" value="%s"  />', $type, $field, $field, $value);
        }
        /**
         * Create input field type range for the width 50%-100%
         *
         * @param [array for field name, default value, input type] $args
         *
         * @return input element for form
         */
        public function settingsFieldInputRange($args)
        {
            $field = $args['field'];
            $default = $args['default'];
            $type = $args['type'];
            // Get the value of this setting
            $value = get_option($field);

            echo '<datalist id="tickmarks">
			<option value="50" label="50%">
			<option value="60">
			<option value="70">
			<option value="80">
			<option value="90">
			<option value="100" label="100%">
		    </datalist><input type="' . $type . '" list="tickmarks" name="' . $field . '"  min="50" max="100" value="' . $value . '" step="10" label="50%" />';
        }

        /**
         * Create input field type radio for the font size
         *
         * @param [array for field name, default value, input type] $args
         *
         * @return input element for form
         */
        public function settingsFieldInputRadio($args)
        {
            $field = $args['field'];
            $default = $args['default'];
            $type = $args['type'];
            // Get the value of this setting
            $value = get_option($field);

            echo '<label for x-smallId>' . __('x-small', 'bizsitesetc_calendar') . '</label><input type="' . $type . '" name="' . $field . '" id="x-smallId" value="x-small"' . checked('x-small', get_option($field), false) . ' />';
            echo '<label for smallId>' . __('small', 'bizsitesetc_calendar') . '</label><input type="' . $type . '" name="' . $field . '" id="smallId" value="small"' . checked('small', get_option($field), false) . ' />';
            echo '<label for mediumId>' . __('medium', 'bizsitesetc_calendar') . '</label><input type="' . $type . '" name="' . $field . '" id="mediumId" value="medium"' . checked('medium', get_option($field), false) . ' />';
            echo '<label for largeId>' . __('large', 'bizsitesetc_calendar') . '</label><input type="' . $type . '" name="' . $field . '" id="largeId" value="large"' . checked('large', get_option($field), false) . ' />';
        }
        /**
         * Create input field type checkbox
         *
         * @param [array for field name, default value, input type] $args
         *
         * @return input element for form
         */
        public function settingsFieldInputCheckbox($args)
        {
            $field = $args['field'];
            $default = $args['default'];
            $type = $args['type'];
            // Get the value of this setting
            $value = get_option($field);
            echo '<input type="' . $type . '" name="' . $field . '" value="1"' . checked(1, get_option($field), false) . ' />';
        }
         /**
          * Create input field type radio for the font size
          *
          * @param [array for field name, default value, input type] $args
          *
          * @return input element for form
          */
        public function settingsFieldInputColor($args)
        {
            $field = $args['field'];
            $default = $args['default'];
            $type = $args['type'];
            $class = $args['input_class'];
            // Get the value of this setting
            $value = get_option($field);
            echo sprintf('<input type="%s" name="%s" id="%s" value="%s" class="%s" />', $type, $field, $field, $value, $class);
        }

         /**
          * Create calendar terms table
          *
          * @param [array for field name, default value, input type] $args
          *
          * @return calendar terms table with text input for admin form
          */
        public function settingsFieldInputTermTable($args)
        {
            // Get the field name from the $args array
            $field = $args['field'];
            $default = $args['default'];
            $type = $args['type'];
            // Get the value of this setting
            $value = get_option($field);

            $output = '<div i="termsTable" >';
            $output .= '<table class="termstable" border="1">';
            $output .= '<caption>' . __('Event Type (Default icon &#x1f4c5) to find icons check ', 'bizsitesetc_calendar') . '<a href="' . esc_url('https://graphemica.com') . '" target="blank">https://graphemica.com</a></caption>';
            $output .= '<tbody>';
            $output .= '<tr><th scope="col"  class="calendar-day-title">' . __('Event Type', 'bizsitesetc_calendar') . '</th><th scope="col"  class="calendar-day-title">Hexadecimal Icon value</th><th>Icon</></tr>';

            $output .= '<tr>';
            $output .= '<th scope="row" class="calendar-day-title">' . $default . '</th>';
            $output .= '<td><input type="' . $type . '" name="' . $field. '" id="' . $field .'" value="' . esc_html($value) . '"  /><td>' . $value . '</td>';
            $output .= '</tr>';
            $output .= '</tbody></table>';
            echo $output;

        } // END public function settingsFieldInputTermTable($args)

        /**
         * Add a menu for the calendar page to  event_post
         *
         * @return mixed
         */
        public function addMenu()
        {
            add_submenu_page(
                'edit.php?post_type=event_post',
                __('Calendar Settings', 'bizsitesetc_calendar'),
                __('Calendar Settings', 'bizsitesetc_calendar'),
                'manage_options',
                'bizsitesetc-calendar',
                array(&$this,'bizsitesetcCalendarSettingsPage')
            );

            add_submenu_page(
                'options.php',
                __('Calendar Settings', 'bizsitesetc_calendar'),
                __('Calendar Settings', 'bizsitesetc_calendar'),
                'manage_options',
                'edit-calendar',
                array(&$this,'bizsitesetcCalendarSettingsPage2')
            );

            add_submenu_page(
                'options.php',
                __('Style Settings', 'bizsitesetc_calendar'),
                __('Style Settings', 'bizsitesetc_calendar'),
                'manage_options',
                'edit-style',
                array(&$this,'bizsitesetcCalendarSettingsPage3')
            );

        } // END public function add_menu()

        /**
         * Menu Callback
         *
         * @return error message
         */
        public function bizsitesetcCalendarSettingsPage3()
        {
            if (!current_user_can('manage_options')) {
                wp_die(__('Yout do not have sufficient permissions to access this page.', 'bizsitesetc_calendar'));
            }
            echo 'Edit Style';
            // Render the settings template
            include sprintf("%s/templates/edit-style.php", dirname(__FILE__));

            // Render the settings template

        } // END public function plugin_settings_page()

        /**
         * Menu Callback
         *
         * @return error message
         */
        public function bizsitesetcCalendarSettingsPage2()
        {
            if (!current_user_can('manage_options')) {
                wp_die(__('You do not have sufficient permissions to access this page.', 'bizsitesetc_calendar'));
            }
            //    echo 'setting page 2';
            // Render the settings template
            include sprintf("%s/templates/edit-page.php", dirname(__FILE__));

            // Render the settings template
            // include sprintf("%s/templates/admin_settings.php", dirname(__FILE__));
        } // END public function plugin_settings_page()

        /**
         * Menu Callback
         *
         * @return error message
         */
        public function bizsitesetcCalendarSettingsPage()
        {
            if (!current_user_can('manage_options')) {
                wp_die(__('You do not have sufficient permissions to access this page.', 'bizsitesetc_calendar'));
            }

            // Render the settings template
            include sprintf("%s/templates/admin_settings.php", dirname(__FILE__));
        } // END public function plugin_settings_page()

    } // END class Plant_Post_Settings
} // END if(!class_exists('Plant_Post_Settings'))
