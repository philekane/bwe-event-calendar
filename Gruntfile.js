module.exports = function( grunt ) {

	'use strict';
	var directoriesConfig = {
        composer: 'vendor',
        composerBin: 'vendor/bin',
        reports: 'logs',
        php: 'event_post'
    };

	// Project configuration
	grunt.initConfig( {
		directories: directoriesConfig,
		pkg: grunt.file.readJSON( 'package.json' ),

		addtextdomain: {
			options: {
				textdomain: 'bizsitesetc_calendar',
			},
			update_all_domains: {
				options: {
					updateDomains: true
				},
				src: [ '*.php', '**/*.php', '!\.git/**/*', '!bin/**/*', '!node_modules/**/*', '!tests/**/*' ]
			}
		},

		wp_readme_to_markdown: {
			your_target: {
				files: {
					'README.md': 'readme.txt'
				}
			},
		},

		makepot: {
			target: {
				options: {
					domainPath: '/languages',
					exclude: [ '\.git/*', 'bin/*', 'node_modules/*', 'tests/*' ],
					mainFile: 'event_post.php',
					potFilename: 'event_post.pot',
					potHeaders: {
						poedit: true,
						'x-poedit-keywordslist': true
					},
					type: 'wp-plugin',
					updateTimestamp: true
				}
			}
		},
		jshint: {
            options: {
                jshintrc: '.jshintrc',
                reporter: require('jshint-stylish')
            },
            all: [
                'Gruntfile.js'
            ]
        },
        jsvalidate: {
            files: [
                'Gruntfile.js'
            ]
        },
        jsonlint: {
            files: [
                '*.json'
            ]
		},
		phplint: {
			options: {
				swapPath: '/tmp'
			},
			//all: ['<%= directories.php %>/**/*.php'	]
			//all: ['*.php']
			//all: ['src/*.php', 'src/base/*.php', 'src/config/*.php', 'src/controller/*.php', 'src/model/*.php']

		},
		phpcs: {
			application: {
			//	dir: '<%= directories.php %>'
				dir: '/'
			},
			options: {
				bin: '<%= directories.composerBin %>/phpcs',
				standard: 'PSR2',
				ignore: 'database',
				extensions: 'php'
			}
		},
		phpunit: {
			classes: {
				dir: '<%= directories.php %>/tests'
			},
			options: {
				bin: '<%= directories.composerBin %>/phpunit',
				bootstrap: 'tests/bootstrap.php',
				staticBackup: false,
				colors: true,
				noGlobalsBackup: false
			}
		}
	} );

	grunt.loadNpmTasks( 'grunt-wp-i18n' );
	grunt.loadNpmTasks( 'grunt-wp-readme-to-markdown' );
	grunt.loadNpmTasks('grunt-contrib-jshint');
	grunt.loadNpmTasks('grunt-phplint');
	grunt.loadNpmTasks('grunt-phpcs');
	grunt.loadNpmTasks('grunt-phpunit');
//	grunt.loadNpmTasks('jsvalidate');
//	grunt.loadNpmTasks('jsonlint');
	grunt.registerTask( 'default', [ 'i18n','readme' ] );
	grunt.registerTask( 'i18n', ['addtextdomain', 'makepot'] );
	grunt.registerTask( 'readme', ['wp_readme_to_markdown'] );

	grunt.util.linefeed = '\n';

    grunt.registerTask('default', [
		'test'
	]);

    grunt.registerTask('test', [
		'phplint',
		'phpcs',
		'phpunit',
		'jshint',
		/*'jsonlint',
		'jsvalidate'*/

    ]);

};
