# Copyright (C) 2019 Phil Kane
# This file is distributed under the same license as the Bizsitesetc Events Calendar package.
msgid ""
msgstr ""
"Project-Id-Version: Bizsitesetc Events Calendar 0.1.0\n"
"Report-Msgid-Bugs-To: https://wordpress.org/support/plugin/event_post\n"
"POT-Creation-Date: 2019-07-25 16:12:23+00:00\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=utf-8\n"
"Content-Transfer-Encoding: 8bit\n"
"PO-Revision-Date: 2019-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"X-Generator: grunt-wp-i18n 0.5.4\n"
"X-Poedit-KeywordsList: "
"__;_e;_x:1,2c;_ex:1,2c;_n:1,2;_nx:1,2,4c;_n_noop:1,2;_nx_noop:1,2,3c;esc_"
"attr__;esc_html__;esc_attr_e;esc_html_e;esc_attr_x:1,2c;esc_html_x:1,2c;\n"
"Language: en\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Poedit-Country: United States\n"
"X-Poedit-SourceCharset: UTF-8\n"
"X-Poedit-Basepath: ../\n"
"X-Poedit-SearchPath-0: .\n"
"X-Poedit-Bookmarks: \n"
"X-Textdomain-Support: yes\n"

#: calendar_settings.php:112
msgid "Event Calendars"
msgstr ""

#: calendar_settings.php:114
msgid "Select Event"
msgstr ""

#: calendar_settings.php:115 calendar_settings.php:712
#: post-types/custom_event_post.php:55
msgid "All Events"
msgstr ""

#: calendar_settings.php:117
msgid " Events"
msgstr ""

#: calendar_settings.php:321
msgid "Today's Calendar"
msgstr ""

#: calendar_settings.php:453
msgid "Show icon on calendar"
msgstr ""

#: calendar_settings.php:592
msgid "Show events selection on calendar"
msgstr ""

#: calendar_settings.php:699
msgid "Calendars"
msgstr ""

#: calendar_settings.php:701
msgid "Calendar"
msgstr ""

#: calendar_settings.php:702
msgid "shortcode"
msgstr ""

#: calendar_settings.php:703
msgid "Author"
msgstr ""

#: calendar_settings.php:704
msgid "Date"
msgstr ""

#: calendar_settings.php:705
msgid "Date Modified"
msgstr ""

#: calendar_settings.php:706
msgid "Edit"
msgstr ""

#: calendar_settings.php:708
msgid "Admin"
msgstr ""

#: calendar_settings.php:709 calendar_settings.php:713
#: calendar_settings.php:720
msgid "Edit Calendar"
msgstr ""

#: calendar_settings.php:733
msgid "Styles"
msgstr ""

#: calendar_settings.php:735
msgid "Style"
msgstr ""

#: calendar_settings.php:736
msgid "Width"
msgstr ""

#: calendar_settings.php:737
msgid "Color"
msgstr ""

#: calendar_settings.php:738
msgid "Font Size"
msgstr ""

#: calendar_settings.php:739
msgid "Edit Style"
msgstr ""

#: calendar_settings.php:830
msgid "x-small"
msgstr ""

#: calendar_settings.php:831
msgid "small"
msgstr ""

#: calendar_settings.php:832
msgid "medium"
msgstr ""

#: calendar_settings.php:833
msgid "large"
msgstr ""

#: calendar_settings.php:880
msgid "Event Type (Default icon &#x1f4c5) to find icons check "
msgstr ""

#: calendar_settings.php:882
msgid "Event Type"
msgstr ""

#: calendar_settings.php:902 calendar_settings.php:903
#: calendar_settings.php:920 calendar_settings.php:921
#: templates/admin_settings.php:2
msgid "Calendar Settings"
msgstr ""

#: calendar_settings.php:929 calendar_settings.php:930
msgid "Style Settings"
msgstr ""

#: calendar_settings.php:947 calendar_settings.php:965
msgid "Yout do not have sufficient permissions to access this page."
msgstr ""

#: calendar_settings.php:985
msgid "You do not have sufficient permissions to access this page."
msgstr ""

#: event_post.php:98
#. translators: %1$s expands to Yoast.
msgid "%1$s Event Blocks"
msgstr ""

#: post-types/custom_event_post.php:53 post-types/custom_event_post.php:77
msgid "Events"
msgstr ""

#: post-types/custom_event_post.php:54
msgid "Event"
msgstr ""

#: post-types/custom_event_post.php:56
msgid "Event Archives"
msgstr ""

#: post-types/custom_event_post.php:57
msgid "Event Attributes"
msgstr ""

#: post-types/custom_event_post.php:58
msgid "Insert into Event"
msgstr ""

#: post-types/custom_event_post.php:59
msgid "Uploaded to this PlEventant"
msgstr ""

#: post-types/custom_event_post.php:61
msgid "Set featured image"
msgstr ""

#: post-types/custom_event_post.php:64
msgid "Filter Events list"
msgstr ""

#: post-types/custom_event_post.php:65
msgid "Events list navigation"
msgstr ""

#: post-types/custom_event_post.php:66
msgid "Events list"
msgstr ""

#: post-types/custom_event_post.php:67
msgid "New Event"
msgstr ""

#: post-types/custom_event_post.php:68
msgid "Add New"
msgstr ""

#: post-types/custom_event_post.php:69
msgid "Add New Event"
msgstr ""

#: post-types/custom_event_post.php:70
msgid "Edit Event"
msgstr ""

#: post-types/custom_event_post.php:71
msgid "View Event"
msgstr ""

#: post-types/custom_event_post.php:72
msgid "View Events"
msgstr ""

#: post-types/custom_event_post.php:73
msgid "Search Events"
msgstr ""

#: post-types/custom_event_post.php:74
msgid "No Events found"
msgstr ""

#: post-types/custom_event_post.php:75
msgid "No Events found in trash"
msgstr ""

#: post-types/custom_event_post.php:76
msgid "Parent Event:"
msgstr ""

#: taxonomy.php:12 taxonomy.php:29
msgid "Event Types"
msgstr ""

#: taxonomy.php:14
msgid "Search Event Type"
msgstr ""

#: taxonomy.php:15
msgid "Popular Event Type"
msgstr ""

#: taxonomy.php:16
msgid "All Event Type"
msgstr ""

#: taxonomy.php:17
msgid "Parent Event Type"
msgstr ""

#: taxonomy.php:18
msgid "Parent Event Type:"
msgstr ""

#: taxonomy.php:19
msgid "Edit Event Type"
msgstr ""

#: taxonomy.php:20
msgid "Update Event Type"
msgstr ""

#: taxonomy.php:21
msgid "View Event Type"
msgstr ""

#: taxonomy.php:22
msgid "Add New Event Type"
msgstr ""

#: taxonomy.php:23
msgid "New Event Type"
msgstr ""

#: taxonomy.php:24
msgid "Separate event type with commas"
msgstr ""

#: taxonomy.php:25
msgid "Add or remove event type"
msgstr ""

#: taxonomy.php:26
msgid "Choose from the most used event type"
msgstr ""

#: taxonomy.php:27
msgid "No event type found."
msgstr ""

#: taxonomy.php:28
msgid "No event type"
msgstr ""

#: taxonomy.php:30
msgid "Event Type list navigation"
msgstr ""

#: taxonomy.php:31
msgid "Event Type list"
msgstr ""

#: taxonomy.php:33
msgid "&larr; Back to Event Type"
msgstr ""

#: templates/edit-page.php:2
msgid "Edit Calendar Settings"
msgstr ""

#: templates/edit-style.php:2
msgid "Edit Style Settings"
msgstr ""

#. Plugin Name of the plugin/theme
msgid "Bizsitesetc Events Calendar"
msgstr ""

#. Plugin URI of the plugin/theme
msgid "https://business-websites-etc.com/event_post"
msgstr ""

#. Description of the plugin/theme
msgid "Custom Event post with shortcode for a calendar"
msgstr ""

#. Author of the plugin/theme
msgid "Phil Kane"
msgstr ""

#. Author URI of the plugin/theme
msgid "https://business-websites-etc.com/phil-kane"
msgstr ""

#: post-types/custom_event_post.php:60
msgctxt "event"
msgid "Featured Image"
msgstr ""

#: post-types/custom_event_post.php:62
msgctxt "event"
msgid "Remove featured image"
msgstr ""

#: post-types/custom_event_post.php:63
msgctxt "event"
msgid "Use as featured image"
msgstr ""

#: taxonomy.php:13
msgctxt "taxonomy general name"
msgid "Event Type"
msgstr ""

#: taxonomy.php:32
msgctxt "event type"
msgid "Most Used"
msgstr ""