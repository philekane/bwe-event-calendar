# BWE_Event_Calendar #
**Contributors:** Phil Kane from https://business-websites-etc.com
**Donate link:** https://example.com/
**Tags:** events, calendar
**Requires at least:** 5.6
**Tested up to:** 5.9
**Requires PHP:** 7.0
**Stable tag:** 0.1.0
**License:** GPLv2 or later
**License URI:** https://www.gnu.org/licenses/gpl-2.0.html

This plugin creates an event custom post with event_type and event_location taxonomies. Use a shortcode to show a calendar with the event.

## Description ##

This plugin is based on a hack in O’reilly’s "PHP Hacks", written by Jack D. Herrington, Create an Interactive Calendar.
This plugin creates a event custom post as well as event_type and
event_location taxonomies. Each event type that is created creates a
shortcode (e.g., [bizSitesEtcEventsCalendar event=books style=0] ),
if a different css style is wanted you can create a style that changes
the width of the calendar (50%-100%) and the color and the font size of the text.
An icon can be set to show on the calendar when editing the calendar,
as a html entity (hexadecimal) like '&#x266C;' for a musical note.
(e.g., [bizSitesEtcEventsCalendar event=music style=1] )


## Installation ##

1. Upload `bwe_events_calendar` to the `/wp-content/plugins/` directory
1. Activate the plugin through the 'Plugins' menu in WordPress
 Or
1. `cd` into your `/wp-content/plugins/` folder and run `git clone https://philekane@bitbucket.org/philekane/bwe-event-calendar.git`
1. Activate the plugin through the 'Plugins' menu in WordPress

## Frequently Asked Questions ##

### Does the event on the calendar show the location of the event? ###

Not at this time, however that is on the todo list.

### Can multiple events be listed on the same day> ###

Yes, it will be listed in order of the time of the events

## Screenshots ##

### 1. This screen shot is the calendar displayed from the shortcode for all events. ###
![This is the first screen shot](./assets/screenshot-1.png)

### 2. This screen shot shows user selecting an event type. ###
![This is the second screen shot](./assets/screenshot-2.png)

### 2. This screen shot shows the calendar settings in the admin section. ###
![This is the third screen shot](./assets/screenshot-3.png)

### 2. This screen shot shows the user editing the style settings. ###
![This is the fourth screen shot](./assets/screenshot-4.png)

### 2. This screen shot shows the edit calendar settings, adding an hexadecimal icon to show on the calendar. ###
![This is the fifth screen shot](./assets/screenshot-5.png)


== Changelog ==

== 1.0.1 ==
* Changed capabilities of changing email transport settings to administrator

= 1.0 =
* Added namespace to plugin.

== Upgrade Notice ==

= 1.0 =
namespace was added to the plugin to avoid clashes between classes
and functions with the same name and to avoid long class names.

## Features ##

1. Interactive html accessible calendar
2. Custom Event Post
3. Taxonomies
	a. Event Types
    b. Event Locations
4. Custom css styles for showing event on calendar
5. Custom icons for event types

