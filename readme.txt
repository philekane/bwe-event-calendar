=== BWE_Event_Calendar ===
Contributors: Phil Kane from https://business-websites-etc.com
Donate link: https://example.com/
Tags: events, calendar
Requires PHP 7.0
Requires at least: 4.5
Tested up to: 5.9
Stable tag: 0.1.0
License: GPLv2 or later
License URI: https://www.gnu.org/licenses/gpl-2.0.html

This plugin creates an event custom post with event_type and event_location
taxonomies. Use a shortcode to show a calendar with the event.

== Description ==

This plugin creates a event custom post as well as event_type and
event_location taxonomies. Each event type that is created creates a
shortcode (e.g., [bizSitesEtcEventsCalendar event=books style=0] ),
if a different css style is wanted you can create a style that changes
the width of the calendar (50%-100%) and the color and the font size of the text.
An icon can be set to show on the calendar when editing the calendar,
as a html entity (hexadecimal) like '&#x1F4D6;' for a musical note.
(e.g., [bizSitesEtcEventsCalendar event=music style=1] )

== Installation ==

1. Upload `bwe_events_calendar` to the `/wp-content/plugins/` directory
1. Activate the plugin through the 'Plugins' menu in WordPress

== Frequently Asked Questions ==

= A question that someone might have =

An answer to that question.

= What about foo bar? =

Answer to foo bar dilemma.

== Screenshots ==

1. screenshot-1.png.
2. screenshot-2.png
3. screenshot-3.png
4. screenshot-4.png
5. screenshot-4.png

== Changelog ==

= 1.0 =
* Added namespace to plugin.

== Upgrade Notice ==

= 1.0 =
namespace was added to the plugin to avoid clashes between classes
and functions with the same name and to avoid long class names.

== Arbitrary section ==

You may provide arbitrary sections, in the same format as the ones above.  This may be of use for extremely complicated
plugins where more information needs to be conveyed that doesn't fit into the categories of "description" or
"installation."  Arbitrary sections will be shown below the built-in sections outlined above.

== A brief Markdown Example ==

Features:

1. Interactive html accessible calendar
2. Custom Event Post
3. Taxonomies
	a. Event Types
    b. Event Locations
4. Custom css styles for showing event on calendar
5. Custom icons for event types

