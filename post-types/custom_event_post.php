<?php
/**
 * Create custom post type Event Post
 */

namespace BWE\EventsCalendar\Post_Types;

if ( ! class_exists( 'Custom_Event_Post' ) ) {
	/**
	 * Custom_Event_Post class
	 */
    class Custom_Event_Post {
        const POST_TYPE = 'event_post';
        /**
         * The Constructor
         */

        public function __construct() {
            // register actions.
            add_action( 'init', array(&$this, 'init' ) );
            add_action( 'admin_init', array( &$this, 'adminInit' ) );

        } // END public function __construct()

        /**
         * Hook into WP's admin_init action hook
         *
         * @return void
         */
        public function adminInit() {

        } // END public function admin_init()

        /**
         * Hook into WP's init action hook
         *
         * @return create post type
         */
        public function init() {
            // Initialize Post Type.
            $this->create_post_type();

        } // END public function init()

        /**
         * Create the post type
         */
        public function create_post_type() {
            $events_calendar_labels = array(
				'name'                  => __( 'Events', 'bizsitesetc_calendar' ),
				'singular_name'         => __( 'Event', 'bizsitesetc_calendar' ),
				'all_items'             => __( 'All Events', 'bizsitesetc_calendar' ),
				'archives'              => __( 'Event Archives', 'bizsitesetc_calendar' ),
				'attributes'            => __( 'Event Attributes', 'bizsitesetc_calendar' ),
				'insert_into_item'      => __( 'Insert into Event', 'bizsitesetc_calendar' ),
				'uploaded_to_this_item' => __( 'Uploaded to this Event', 'bizsitesetc_calendar' ),
				'featured_image'        => _x( 'Featured Image', 'event', 'bizsitesetc_calendar' ),
				'set_featured_image'    => __( 'Set featured image', 'bizsitesetc_calendar' ),
				'remove_featured_image' => _x( 'Remove featured image', 'event_post', 'bizsitesetc_calendar' ),
				'use_featured_image'    => _x( 'Use as featured image', 'event_post', 'bizsitesetc_calendar' ),
				'filter_items_list'     => __( 'Filter Events list', 'bizsitesetc_calendar' ),
				'items_list_navigation' => __( 'Events list navigation', 'bizsitesetc_calendar' ),
				'items_list'            => __( 'Events list', 'bizsitesetc_calendar' ),
				'new_item'              => __( 'New Event', 'bizsitesetc_calendar' ),
				'add_new'               => __( 'Add New', 'bizsitesetc_calendar' ),
				'add_new_item'          => __( 'Add New Event', 'bizsitesetc_calendar' ),
				'edit_item'             => __( 'Edit Event', 'bizsitesetc_calendar' ),
				'view_item'             => __( 'View Event', 'bizsitesetc_calendar' ),
				'view_items'            => __( 'View Events', 'bizsitesetc_calendar' ),
				'search_items'          => __( 'Search Events', 'bizsitesetc_calendar' ),
				'not_found'             => __( 'No Events found', 'bizsitesetc_calendar' ),
				'not_found_in_trash'    => __( 'No Events found in trash', 'bizsitesetc_calendar' ),
				'parent_item_colon'     => __( 'Parent Event:', 'bizsitesetc_calendar' ),
				'menu_name'             => __( 'Events', 'bizsitesetc_calendar' ),
            );

            $events_calendar_args = array(
				'labels'                => $events_calendar_labels,
				'orderby'               => 'title',
				'order'                 => 'ASC',
				'public'                => true,
				'hierarchical'          => false,
				'show_ui'               => true,
				'show_in_nav_menus'     => true,
				'supports'              => array(
					'title',
					'editor',
					'excerpt',
					'thumbnail',
					'custom-fields',
				),
				'has_archive'           => true,
				'rewrite'               => array(
					'slug' => 'events',
				),
				'query_var'             => true,
				'menu_position'         => 5,
				'menu_icon'             => 'dashicons-calendar-alt',
				'show_in_rest'          => true,
				'rest_base'             => 'event',
				'rest_controller_class' => 'WP_REST_Posts_Controller',
				'taxonomies'            => array(
					'event-types',
				),
				'template'              => array(
					array( 'core/heading' ),
					array( 'core/paragraph' ),
					//array( 'event-post-location/meta-block' ),
					array( 'event-post-calendarcopy/meta-block' ),
					array( 'event-post-date/meta-block' ),
					array( 'event-post-time/meta-block' ),
					array( 'event-post-end-time/meta-block' ),
				),
				'template_lock' => 'all',
            );

            register_post_type( self::POST_TYPE, $events_calendar_args );
        }

    }
}
