<?php
/**
 * Uninstall bwetc event posts
 */

if ( ! defined( 'WP_UNINSTALL_PLUGIN' ) ) {
	exit;
}
/* remove editors permission to edit options*/
//$role = get_role( 'editor' );
//$role->remove_cap( 'manage_options' );

/**
 * Delete all options that have been created (styles; font-sizes, content-color
 * width, icons)
 *
 */
foreach ( wp_load_alloptions() as $option => $value ) {
    if ( strpos( $option, 'bwetc_calendar_' ) === 0 ) {
        delete_option( $option );
    }
	if ( strpos( $option, 'event-locations_children' ) === 0 ) {
        delete_option( $option );
    }
	if ( strpos( $option, 'event-types_children' ) === 0 ) {
        delete_option( $option );
    }
}


/**
 *  Delete Terms of Taxonomy
 */
/*
$bwe_taxonomies = array( 'event-locations', 'event-types' );

foreach ( $bwe_taxonomies as $bwe_taxonomy ) {

	$terms = get_terms(
		array(
			'taxonomy' => $bwe_taxonomy,
		),
	);

	global $wpdb;
	foreach ( $terms as $bwe_term ) {
		$wpdb->delete( $wpdb->term_taxonomy, array( 'term_taxonomy_id' => $bwe_term->term_taxonomy_id ) );
		$wpdb->delete( $wpdb->term_relationships, array( 'term_taxonomy_id' => $bwe_term->term_taxonomy_id ) );
		$wpdb->delete( $wpdb->terms, array( 'term_id' => $bwe_term->term_id ) );
	}
	*/
	/**
	 * Unregister taxonomy
	 */
	//unregister_taxonomy( $bwe_taxonomy );
	// Delete Taxonomy
	//$wpdb->delete( $wpdb->term_taxonomy, array( 'taxonomy' => $bwe_taxonomy ), array( '%s' ) );
}


/**
 *
 * Delete posts
 */

$bwe_posts = get_posts(
	array(
		'numberposts' => -1,
		'post_type'   => 'event_post',
		'post_status' => 'any',
		'posts_per_page' => -1,
	),
);
if ( $bwe_posts ) {
	foreach ( $bwe_posts as $bwe_post ) {
		wp_delete_post( $bwe_post->ID, true ); // force_delete set to true and permanently deleted.
	//	wp_delete_object_term_relationships( $bwe_post->ID, $bwe_taxonomies );
	}
}

?>
