<?php
/**
 * Functions to register client-side assets (scripts and stylesheets) for the
 * Gutenberg block.
 *
 * @package bizsitesetc_calendar
 */

/**
 * Registers all block assets so that they can be enqueued through Gutenberg in
 * the corresponding context.
 *
 * @see https://wordpress.org/gutenberg/handbook/blocks/writing-your-first-block-type/#enqueuing-block-scripts
 */
// register custom meta tag field
function event_post_register_meta()
{
    register_meta(
		'post', 'event_post_calendarcopy', array(
        'object_subtype' => 'event_post',
        'show_in_rest' => true,
        'single' => true,
        'type' => 'string'
        )
	);
	register_meta(
		'post', 'event_post_date', array(
		'object_subtype' => 'event_post',
		'show_in_rest' => true,
		'single'           => true,
		'type'         => 'string'
		)
	);
	register_meta(
		'post', 'event_post_time', array(
		'object_subtype' => 'event_post',
		'show_in_rest' => true,
		'single'           => true,
		'type'         => 'string'
		)
	);
	register_meta(
		'post', 'event_post_end_time', array(
		'object_subtype' => 'event_post',
		'show_in_rest' => true,
		'single'           => true,
		'type'         => 'string'
		)
	);
	/*
	register_meta(
		'post', 'event_post_location', array(
		'object_subtype' => 'event_post',
		'show_in_rest' => true,
		'single'           => true,
		'type'         => 'string'
		)
	);
	*/
}
add_action('init', 'event_post_register_meta');

function event_post_enqueue()
{
    $event_editor_css = 'event_post_meta/editor.css';
    wp_enqueue_style(
        'editor',
        plugins_url($event_editor_css, __FILE__),
        array()
    );

	$index_js = 'event_post_meta/index.js';
	wp_enqueue_script(
		'index',
		plugins_url( $index_js, __FILE__ ),
		array( 'wp-blocks', 'wp-element', 'wp-components', 'wp-editor', 'wp-i18n' )
	);

}
add_action( 'enqueue_block_editor_assets', 'event_post_enqueue' );



/**
 *  Enqueues the style.css for the frontend
 */
function event_post_style_enqueue()
{
    $style_css = 'event_post_meta/style.css';
    wp_enqueue_style(
        'style',
        plugins_url($style_css, __FILE__),
        array()
    );
}
add_action('enqueue_block_assets', 'event_post_style_enqueue');
