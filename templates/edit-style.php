<div class="wrap">
    <h2><?php echo __('Edit Style Settings', 'bizsitesetc_calendar') ?></h2>

	   <form method="post" action="options.php" autocomplete="off">
	  <?php @settings_fields('bizsitesetc_style-group'); ?>
		<?php @do_settings_fields('bizsitesetc_style-group',''); ?>
		<?php do_settings_sections('bizsitesetc_style'); ?>

		<?php
		//if(isset($_GET['action'])){
			@submit_button(); ?>
		</form>
		<?php // } ?>
</div>
<?php
		$short = '[bizSitesEtcEventsCalendar]';
		echo do_shortcode( $short );
		?>
