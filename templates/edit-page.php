<div class="wrap">
    <h2><?php echo __('Edit Calendar Settings', 'bizsitesetc_calendar') ?></h2>

	   <form method="post" action="options.php" autocomplete="off">
	  <?php @settings_fields('bizsitesetc_event-group'); ?>
		<?php @do_settings_fields('bizsitesetc_event-group',''); ?>
		<?php do_settings_sections('bizsitesetc_event'); ?>

		<?php
		//if(isset($_GET['action'])){
			@submit_button(); ?>
		</form>
		<?php // } ?>
</div>
<?php
		$short = '[bizSitesEtcEventsCalendar]';
		echo do_shortcode( $short );
		?>
