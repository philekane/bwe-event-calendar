<?php
/**
 * Registers the `Events Type` taxonomy,
 * for use with 'event', 'events'.
 *
 * @return mixed
 *
 */

//namespace BWE\EventsCalendar;

function Events_calendar_taxonomies() {

    $event_type_labels = array(
        'name'                       => __( 'Event Types', 'bizsitesetc_calendar' ),
        'singular_name'              => _x( 'Event Type', 'taxonomy general name', 'bizsitesetc_calendar' ),
        'search_items'               => __( 'Search Event Type', 'bizsitesetc_calendar' ),
        'popular_items'              => __( 'Popular Event Type', 'bizsitesetc_calendar' ),
        'all_items'                  => __( 'All Event Type', 'bizsitesetc_calendar' ),
        'parent_item'                => __( 'Parent Event Type', 'bizsitesetc_calendar' ),
        'parent_item_colon'          => __( 'Parent Event Type:', 'bizsitesetc_calendar' ),
        'edit_item'                  => __( 'Edit Event Type', 'bizsitesetc_calendar' ),
        'update_item'                => __( 'Update Event Type', 'bizsitesetc_calendar' ),
        'view_item'                  => __( 'View Event Type', 'bizsitesetc_calendar' ),
        'add_new_item'               => __( 'Add New Event Type', 'bizsitesetc_calendar' ),
        'new_item_name'              => __( 'New Event Type', 'bizsitesetc_calendar' ),
        'separate_items_with_commas' => __( 'Separate event type with commas', 'bizsitesetc_calendar' ),
        'add_or_remove_items'        => __( 'Add or remove event type', 'bizsitesetc_calendar' ),
        'choose_from_most_used'      => __( 'Choose from the most used event type', 'bizsitesetc_calendar' ),
        'not_found'                  => __( 'No event type found.', 'bizsitesetc_calendar' ),
        'no_terms'                   => __( 'No event type', 'bizsitesetc_calendar' ),
        'menu_name'                  => __( 'Event Types', 'bizsitesetc_calendar' ),
        'items_list_navigation'      => __( 'Event Type list navigation', 'bizsitesetc_calendar' ),
        'items_list'                 => __( 'Event Type list', 'bizsitesetc_calendar' ),
        'most_used'                  => _x( 'Most Used', 'event type', 'bizsitesetc_calendar' ),
        'back_to_items'              => __( '&larr; Back to Event Type', 'bizsitesetc_calendar' ),
    );

    $event_type_args = array(
        'labels'                => $event_type_labels,
        'hierarchical'          => true,
        'public'                => true,
        'show_in_nav_menus'     => true,
        'show_ui'               => true,
        'show_admin_column'     => true,
        'query_var'             => true,
        'rewrite'               => true,
        'capabilities'          => array(
            'manage_terms' => 'edit_posts',
            'edit_terms' => 'edit_posts',
            'delete_terms' => 'edit_posts',
            'assign_terms' => 'edit_posts',
        ),
        'show_in_rest'          => true,
        'rest_base'             => 'event-types',
        'rest_controller_class' => 'WP_REST_Terms_Controller',
    );

	register_taxonomy( 'event-types', array( 'event_post' ), $event_type_args );

}

add_action( 'init', 'Events_calendar_taxonomies' );

/**
 * Registers the `Events Type` taxonomy,
 * for use with 'event', 'events'.
 *
 * @return mixed
 *
 */
function events_calendar_location_taxonomy()
{

    $event_location_labels = array(
        'name'                       => __( 'Locations', 'bizsitesetc_calendar' ),
        'singular_name'              => _x( 'Location', 'taxonomy general name', 'bizsitesetc_calendar' ),
        'search_items'               => __( 'Search Location', 'bizsitesetc_calendar' ),
        'popular_items'              => __( 'Popular Location', 'bizsitesetc_calendar' ),
        'all_items'                  => __( 'All Locations', 'bizsitesetc_calendar' ),
        'parent_item'                => __( 'Parent Location', 'bizsitesetc_calendar' ),
        'parent_item_colon'          => __( 'Parent Location:', 'bizsitesetc_calendar' ),
        'edit_item'                  => __( 'Edit Location', 'bizsitesetc_calendar' ),
        'update_item'                => __( 'Update Location', 'bizsitesetc_calendar' ),
        'view_item'                  => __( 'View Location', 'bizsitesetc_calendar' ),
        'add_new_item'               => __( 'Add New Location', 'bizsitesetc_calendar' ),
        'new_item_name'              => __( 'New Location', 'bizsitesetc_calendar' ),
        'separate_items_with_commas' => __( 'Separate location with commas', 'bizsitesetc_calendar' ),
        'add_or_remove_items'        => __( 'Add or remove lkocation', 'bizsitesetc_calendar' ),
        'choose_from_most_used'      => __( 'Choose from the most used location', 'bizsitesetc_calendar' ),
        'not_found'                  => __( 'No location found.', 'bizsitesetc_calendar' ),
        'no_terms'                   => __( 'No location', 'bizsitesetc_calendar' ),
        'menu_name'                  => __( 'Locations', 'bizsitesetc_calendar' ),
        'items_list_navigation'      => __( 'Location list navigation', 'bizsitesetc_calendar' ),
        'items_list'                 => __( 'Location list', 'bizsitesetc_calendar' ),
        'most_used'                  => _x( 'Most Used', 'location', 'bizsitesetc_calendar' ),
        'back_to_items'              => __( '&larr; Back to Location', 'bizsitesetc_calendar' ),
    );

    $event_location_args = array(
        'labels'                => $event_location_labels,
        'hierarchical'          => true,
        'public'                => true,
        'show_in_nav_menus'     => true,
        'show_ui'               => true,
        'show_admin_column'     => true,
        'query_var'             => true,
        'rewrite'               => true,
        'capabilities'          => array(
            'manage_terms' => 'edit_posts',
            'edit_terms'   => 'edit_posts',
            'delete_terms' => 'edit_posts',
            'assign_terms' => 'edit_posts',
        ),
        'show_in_rest'          => true,
        'rest_base'             => 'locations',
        'rest_controller_class' => 'WP_REST_Terms_Controller',
    );

	register_taxonomy( 'event-locations', array( 'event_post' ), $event_location_args );

}

add_action( 'init', 'events_calendar_location_taxonomy' );
